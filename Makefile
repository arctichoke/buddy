
all:
	cd buddy_src; make;
	cd mydash_src; make;
	cp buddy_src/*.so .;
	cp mydash_src/*.so .;
	cp mydash_src/mydash .;
clean:
	cd buddy_src; make clean;
	cd mydash_src; make clean;
	rm -f *.so mydash;

