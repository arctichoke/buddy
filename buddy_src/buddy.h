#ifndef __BUDDY_H
#define __BUDDY_H

#include <stddef.h>
#include <pthread.h>
#include <limits.h>
#include <string.h>
#include <stddef.h>

#define DEFAULT_ORDER       (20)
#define IS_POW2(x)      ((x) && !((x) & ((x) - 1)))        ///< Checks if number is
#define __align(x, n)   (((x) + ((n)-1)) & ~((n) -1))
#define BLOCK_SIZE      sizeof(block_t) //offsetof(block_t, next)

typedef unsigned int long   offset_t;

/**!
 * @brief Rounds a number up to the nearest power of 2.
 */
static inline size_t roundup_pow2(size_t x) {
    x--;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;
    x |= x >> 32;
    x++;

    return x;
}

typedef struct _block {
        size_t size;
        int free;
        int order;
        struct _block *next, *prev;
} block_t;

typedef struct _list {
        size_t num;                     ///< Number of items in list
        block_t *head;
} list_t;

/**!
 * @brief returns the minimal order that can fit a size.
 */
static inline int find_order(size_t n) {
    return ffs(roundup_pow2(n)) - 1;
}

void buddy_init(size_t size);
void *buddy_calloc(size_t num, size_t size);
void *buddy_malloc(size_t size);
void *buddy_realloc(void *ptr, size_t size);
void buddy_free(void *ptr);
void printBuddyLists();
void *malloc(size_t size);
void free(void *ptr);
void *calloc(size_t nmemb, size_t size);
void *realloc(void *ptr, size_t size);
#endif
