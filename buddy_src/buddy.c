#include <unistd.h>
#include <stdint.h>
#include <errno.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stddef.h>

#include "buddy.h"

/**!
 * Local Variables.
 */
/* not static to check data in unit test */
list_t buddy_list[DEFAULT_ORDER + 1]; /* order 0 to DEFAULT_RODER */
static block_t *ptr_base;

pthread_mutex_t buddy_mutex = PTHREAD_MUTEX_INITIALIZER;

/* Adds to the list */
static void list_add(list_t *list, block_t *block)
{
    block->prev = block->next = NULL;
    block_t *b = list->head;
    list->head = block;
    block->next = b;

    if (b != NULL)
        b->prev = list->head;

    list->num++;
}


/* Removes first element from the list */
block_t *list_remove_first(list_t *list) {
    block_t *b = list->head;

    if (list->num != 0) {
        list->head = b->next;

        if (list->head != NULL)
            list->head->prev = NULL;

        list->num--;
    }

    return b;
}

/* remove any element from the list */
void list_remove(list_t *list, block_t *block) {
    block_t *b = list->head;

    while(b != NULL && (b != block) ) {
        b = b->next;
    }

    if (b != NULL) {

        /* Removing from head */
        if (b == list->head) {
            list->head = b->next;
            if (list->head != NULL)
                list->head->prev = NULL;
        } else {
            if (b->prev != NULL)
                b->prev->next = b->next;

            if (b->next != NULL)
                b->next->prev = b->prev;
        }
        list->num--;
    }
}

/**!
 * @brief initializes the buddy system with the size
 */
void buddy_init(size_t size) {
    int i;
    int order;

    /* make sure that the requested size is always power of  2 */
    if (0 == size)
        size = (1UL << DEFAULT_ORDER);

    order = find_order(size);
    assert(order <= DEFAULT_ORDER);

    /* If the requested size is greater than max order fail.*/
    if ((1UL << DEFAULT_ORDER) < size) {
        errno = ENOMEM;
        return;
    }

    pthread_mutex_lock(&buddy_mutex);

    ptr_base = sbrk(0); ///< Current break address.

    //< 3 extra byte so it can be always be aligned in 4 byte boundary.
    if (sbrk(3 + size) == (void *)-1) {
        pthread_mutex_unlock(&buddy_mutex);
        errno = ENOMEM;
        return;
    }

    ptr_base = (void *)__align((intptr_t) ptr_base, 4);

    /* Add the memory block to the appropriate list */
    for (i = 0; i <=  DEFAULT_ORDER; i++) {
        if ((1 << i) == size) {
            ptr_base->order = i;
            list_add(&buddy_list[i], ptr_base);
            break;
        }
    }

    pthread_mutex_unlock(&buddy_mutex);
}

void *buddy_calloc(size_t num, size_t size) {
    size = size * num;
    char *p = buddy_malloc(size);
    if (p != NULL)
        memset(p, 0, size);

    return p;
}

/**!
 * @brief allocate size bytes in the heap.
 */
void *buddy_malloc(size_t size) {
    size_t len;
    size_t i;
    block_t *block, *buddy_block;
    list_t *list;

    if (0 >= size)
        return NULL;

    len = size;
    size = size + sizeof(block_t);

    /* fix the size to power of 2 if required. */
    if (!IS_POW2(size))
        size = roundup_pow2(size);

    int order = ffs(size) - 1; /* first bit set is the order. */

    /* If the order is more than than DEFAULT_RODER return NULL */
    if (order > DEFAULT_ORDER) {
        errno = ENOMEM;
        return NULL;
    }

    for (i = order; i <= DEFAULT_ORDER; i++) {
        /* try allocate the block from the same order list */
        list = &buddy_list[i];

        /* If list is empty continue */
        if (list->num == 0)
            continue;

        block = list_remove_first(list);

        /* Trim the block if it is of higher order */
        while (i > order) {
            i--;
            buddy_block = (block_t *)((intptr_t)block + (1UL << i));
            buddy_block->free = 1;
            buddy_block->order = i;
            list_add(&buddy_list[i], buddy_block);
        }

        block->order = order;
        block->free = 0;
        block->size = len;
        return &(block->next);

    }

    errno = ENOMEM;
    return NULL;
}

/**!
 * @brief Returns block address from the user pointer.
 */
static block_t *get_block(void *ptr) {
    char* p = (char *)ptr;
    p = p - offsetof(block_t, next);
    return (block_t *)p;
}


/**!
 * @brief Creates block of size and copy data from ptr to it.
 */
void *buddy_realloc(void *ptr, size_t size) {
    char *p;
    block_t *block;

    /* Implementation from specification for realloc. */
    if (size == 0) {
        if (ptr != NULL) {
            buddy_free(ptr);
            return NULL;
        }
        return NULL;
    }

    p = buddy_malloc(size);
    if (ptr == NULL) {
        return p; /* Equivalent to malloc */
    }

    if (p != NULL) {
        block = get_block(ptr);
        memcpy(p, ptr, block->size);
        buddy_free(ptr);
        return p;
    }

    errno = ENOMEM;
    return NULL;
}

/**!
 * @brief Returns the address of the buddy.
 */
static inline block_t *buddy_addr(block_t *block, size_t order) {
    intptr_t base = (intptr_t) ptr_base;
    intptr_t b = (intptr_t) block - base;
    b ^= 1UL << order;
    return (block_t*)(base + b);
}

/**
 * @brief Frees the memory.
 */
void buddy_free(void *ptr) {
    block_t *block, *buddy;
    int order;

    if (ptr == NULL)
        return;

    block = get_block(ptr); /* Address of the block */
    order = block->order;

    /* merge and move up the order */
    while (order < DEFAULT_ORDER) {
        buddy = buddy_addr(block, order);

        if (buddy->free == 0)
            break;
        if (buddy->order != order)
            break;

        list_remove(&buddy_list[order],buddy);
        if (buddy < block)
            block = buddy;

        block->free = 1;
        order++;
        block->order = order;
    }

    /* Adds the merged block to the right order */
    block->order = order;
    block->free = 1;
    list_add(&buddy_list[order], block);
}

void printBuddyLists() {
    int i;
    block_t *b;
    for (i = 0; i <= DEFAULT_ORDER; i++) {
        printf("Order %d: ",i);
        b = buddy_list[i].head;
        while (b != NULL) {
            printf ("%p ", (void *)b);
            b = b->next;
        }
        printf("\n");
    }
}

void *malloc(size_t size) {
    void *p;
    if (ptr_base == NULL) {
        buddy_init(0);
    }
    pthread_mutex_lock(&buddy_mutex);
    p = buddy_malloc(size);
    pthread_mutex_unlock(&buddy_mutex);
    return p;
}

void free(void *ptr) {
    pthread_mutex_lock(&buddy_mutex);
    buddy_free(ptr);
    pthread_mutex_unlock(&buddy_mutex);
}

void *calloc(size_t nmemb, size_t size) {
    void *p;
    pthread_mutex_lock(&buddy_mutex);
    p = buddy_calloc(nmemb, size);
    pthread_mutex_unlock(&buddy_mutex);
    return p;
}

void *realloc(void *ptr, size_t size) {
    void *p;
    pthread_mutex_lock(&buddy_mutex);
    p = buddy_realloc(ptr, size);
    pthread_mutex_unlock(&buddy_mutex);
    return p;
}
