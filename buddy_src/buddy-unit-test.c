
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "buddy.h"


#define MAX_REQUEST 4096
#define MAX_ITEMS 100

#define SILENT 0
#define TERSE  1
#define VERBOSE  2
#define INTERACTIVE 3

int verbosity = TERSE;

#define FAILED 1
#define PASSED 0

/* Global variable. Defined in buddy.c */
extern list_t buddy_list[DEFAULT_ORDER + 1];
static void printAndClear()
{
	printBuddyLists();
	char ch = getchar();
	system("clear");
	if (ch == 'q')
		exit(0);
}


void simpleFreeTest()
{
	char *x;
	
	// buddy_malloc one byte to make buddy system
	// split all the way down
	x = (char *) buddy_malloc(sizeof(char));
	if (x == NULL) {
		fprintf(stderr,"ERROR! Buddy system failed to allocate 1 byte.\n");
		exit(1);
	}
	if (verbosity > 0) {
		printf("Buddy system succeeding in allocating 1 byte. \n");
	}
	if (verbosity > 1) {
		printf("Buddy system lists after malloc'ing 1 byte.\n");
		printAndClear();
	}

	// buddy_free should make all the blocks merge back into one block
	buddy_free(x);
	if (verbosity > 0) {
		printf("Buddy system succeeding in free'ing 1 byte. \n");
	}
	if (verbosity > 1) {
		printf("Buddy system lists after free'ing the block .\n");
		printAndClear();
	}
}


void maxAllocationTest()
{
	void *ptr;
	size_t count;
	
	//buddy_malloc larger and larger blocks until memory is exhausted
	count = 1;
	for (;;) {
		ptr = buddy_malloc(count);
		if (ptr == NULL) {
			perror("TestBuddy");
			if (verbosity > 0) 
				printf("ERROR! buddy_malloc'd failed to allocate a block of size %lu\n", count);
			if (verbosity > 1) printAndClear();
			return;
		} else {
			if (verbosity > 0) 
				printf("buddy_malloc'd a block of size %lu\n", count);
			if (verbosity > 1) printAndClear();
		}
		count = count * 10;
        buddy_free(ptr);
	}	
}


void printTest(char *name) {
        printf("Running test: %s\n", name);
}

void printResult(int r) {
        if (r == PASSED)
                printf("PASSED\n");
        else
                printf("FAILED\n");
}

void printMsg(char *msg) {
    printf("\n%s\n", msg);
}

/**!
 * allocates random bytes.
 */
int test_malloc_free_random() {
    int rnd, i;
    char *p;
    printTest("Malloc and free random size");
    for (i = 0; i < 10000; i++) {
        rnd = random ()  % 1000;
        p = buddy_malloc(rnd);
        if (p != NULL) {
            memset(p, 1, rnd);
            buddy_free(p);
        }
    }

    /* Check if everything merged */
    assert(buddy_list[DEFAULT_ORDER].num != 0);
    printResult(0);
    return 0;
}

/**!
 * Allocate zero bytes.
 */
int test_zero() {
    char *p;
    printTest("Alocate zero byte");
    p = (char *) buddy_malloc(0);
    if (p != NULL) {
        printResult(FAILED);
        return FAILED;
    }
    printResult(PASSED);
    return 0;
}

/**!
 *
 * Malloc and free memory.
 */
int test_malloc_free() {
        size_t i;
        char *p;

        printTest("Malloc and free bytes");
        for(i = 1; i < 100; i++) {
            /* Malloc and free 1 byte */
            p = (char *)buddy_malloc(i);
            if (p != NULL) {
                memset(p, 1, i);        /* Write to the memory */
                buddy_free(p);
            }
        }

        /* Check if everything merged */
        assert(buddy_list[DEFAULT_ORDER].num != 0);
        printResult(PASSED);
        return PASSED;
}

/**!
 * Allocate maximum memory
 */
int test_maximum() {
    char *p;
    printTest("Alloc too big size");
    p = buddy_malloc(1UL << (DEFAULT_ORDER - 1));
    if (p == NULL) {
        printResult(FAILED);
        return FAILED;
    }
    buddy_free(p);

    /* Check if everything merged */
    assert(buddy_list[DEFAULT_ORDER].num != 0);
    printResult(PASSED);
    return PASSED;
}

/**!
 * Allocate all memory and allocate another.
 */
int test_starve() {
    char *p, *q;
    printTest("Alloc after using consuming all mem");
    p = buddy_malloc(1UL << (DEFAULT_ORDER - 1));
    if (p == NULL) {
        printResult(FAILED);
        return FAILED;
    }
    q = (char *)buddy_malloc(1);
    if (q != NULL) {
        printResult(FAILED);
        buddy_free(p);
        buddy_free(q);
        return FAILED;
    }

    buddy_free(p);

    /* Check if everything merged */
    assert(buddy_list[DEFAULT_ORDER].num != 0);
    printResult(PASSED);
    return PASSED;
}

/**!
 * Allocate memory and check if it is zero initialized.
 */
int test_calloc() {
    size_t i, j, k;
    char *p;
    printTest("Calloc test and check zero");
    for (i = 1; i < 1000; i++) {
        for (j = 1; j < 10; j++) {
            p = buddy_calloc(i,j);
            if (p != NULL) {
                for (k = 0; k < i*j; k++) {
                    if (p[0] != 0) {
                        printResult(FAILED);
                        return FAILED;
                    }
                }
                buddy_free(p);
            }
        }
    }

    assert(buddy_list[DEFAULT_ORDER].num != 0);
    printResult(PASSED);
    return PASSED;
}

/**!
 * Allocate bigger wave.
 */
int test_realloc_bigger() {
    char *p;
    size_t size = 10;

    printTest("Realloc bigger");
    for (size = 1; size < 1000; size++) {
        p = (char *)buddy_malloc(size);
        if (p != NULL) {
            memset(p, 1, size);
            p = (char *)buddy_realloc(p, size*2);
            if (p != NULL) {
                memset(p, 1, size*2);
                buddy_free(p);
            }
        }
    }

    /* Check if everything merged. */
    assert(buddy_list[DEFAULT_ORDER].num != 0);
    printResult(PASSED);
    return PASSED;
}

/**!
 * Reallocate and check if data is valid.
 */
int test_realloc_data() {
    char *p,*q;

    printTest("Realloc and check data");
    p = buddy_malloc(10);
    memset(p, 1, 10);
    q = buddy_malloc(10);
    memset(q, 1, 10);

    if( 0 != memcmp(p,q,10)) {
        printResult(FAILED);
        return FAILED;
    }

    p = buddy_realloc(p, 5);
    q = buddy_realloc(q, 20);

    if (p != NULL && q != NULL) {
        if (0 != memcmp(p,q,5)) {
            printResult(FAILED);
            return FAILED;
        }
        buddy_free(p);
        buddy_free(q);
    }

    /* check if everything merged */
    assert(buddy_list[DEFAULT_ORDER].num != 0);
    printResult(PASSED);
    return PASSED;
}

int main(int argc, char *argv[])
{
    if (argc < 2) {
        fprintf(stderr, "Usage: %s {silent|terse|verbose|interactive}\n", argv[0]);
        exit(1);
    }
    if (argv[1][0] == 's') {
        verbosity = SILENT;
    } else if (argv[1][0] == 't') {
        verbosity = TERSE;
    } else if (argv[1][0] == 'v') {
        verbosity = VERBOSE;
    } else if (argv[1][0] == 'i') {
        verbosity = INTERACTIVE;
        setvbuf(stdin, NULL, _IONBF, 0);
    }

	system("clear");

	buddy_init(0);	
	if (verbosity > 0) {
		printf("Buddy system initialized.\n");
	}
	if (verbosity > 1) {
		printf("Buddy system lists after initialization.\n");
		printAndClear();
	}

	simpleFreeTest();

	maxAllocationTest();

    /* test malloc */
    test_zero();
    test_malloc_free();
    test_maximum();
    test_malloc_free_random();
    test_starve();

    /* test calloc */
    test_calloc();

    /* test realloc */
    test_realloc_bigger();
    test_realloc_data();
    exit(0);
}

