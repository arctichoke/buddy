#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <stddef.h>
#include <getopt.h>

#include "dash_types.h"
#include "builtin_cmd.h"
#include "arguments.h"
#include "job.h"
#include "ourhdr.h"

extern char *git_version(void);

extern size_t CMD_TBL_SIZE;
extern Cmd_t builtin_cmd_tbl[];

static EXIT_CODE exit_code;               ///< Exit code for most recent command

/*!
 * @brief Command handler function.
 *
 * @param Argument string to command from user.
 */
static void call_cmd(char *line)
{
    size_t i = 0;
    Arg_t *arg = parse_arg(line);
    free(line);
    if (arg == NULL)
    {
        printf("Arguments mismatched");
        return;
    }

    // Looks at command table for any internal command
    while ( i < CMD_TBL_SIZE - 1 )
    {
        if(0 == strcmp(arg->buf[0], builtin_cmd_tbl[i].name))
        {
            exit_code = builtin_cmd_tbl[i].handler(arg->buf[0], arg);
            return;
        }

        i++;
    }

    // Calls the default handler
    exit_code = builtin_cmd_tbl[CMD_TBL_SIZE-1].handler(arg->buf[0], arg);

}

/*!
 * @brief Displays dash prompt and asks user for input.
 *
 * @param  p: prompt displayed to user.
 */
static void mydash_prompt(char *p)
{
    char *prompt = p;
    char *line;

    using_history();
    while ((line = readline(prompt)))
    {
        if (strlen(line) > 0)
        {
            add_history(line);
        }
        else
        {
            // Nothing supplied by user
            free(line);
            line = (char *)malloc(sizeof(char) * 20);
            strncpy(line, "report_jobs", 11);
            line[11] = '\0';
        }

        call_cmd(line);
    }

    jobs_destroy();
}

/*!
 * Arguments the dash command accepts.
 */
static struct option long_options[] = {
    {"version", 0, 0, 'v'},
    {NULL, 0, 0, 0}
};

int main(int argc, char *argv[])
{
    char *prompt;
    char c;

    if (argc > 1)
    {
        while((c = getopt_long(argc, argv, "v", long_options, NULL)) != 1)
        {
            switch (c)
            {
              case 'v':
                  printf("Version %s\n", git_version());
                  exit(EXIT_SUCCESS);
              default:
                  break;
            }
        }
    }

    prompt = getenv("DASH_PROMPT");

    if (!prompt)
        prompt = "mydash>";

    jobs_init();
    mydash_prompt(prompt);

    return 0;
}
