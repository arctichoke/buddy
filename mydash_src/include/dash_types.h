/*!
 *
 */
#ifndef __DASH_TYPES_H
#define __DASH_TYPES_H

#include <stdint.h>
#include "arguments.h"

typedef uint8_t EXIT_CODE;              ///< Exit status for command handler

/*!
 * Defines the command handler table structure.
 */
typedef struct {
        char *name;                                 ///< Name of the command
        char *desc;                                 ///< Description of command
        EXIT_CODE (*handler)(char *name, Arg_t *);  ///< Command handler
} Cmd_t;

#endif
