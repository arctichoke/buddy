/*!
 * @file: builtin_cmd.h
 * @author: Suraj Deuja
 */
#ifndef __BUILTIN_CMD_H
#define __BUILTIN_CMD_H

#include <stddef.h>

#include "dash_types.h"

/*!
 * @brief Command to change directory.
 *
 * @param name : Name of the command.
 * @param arg  : Argument to the command.
 *
 * @return Exit status
 */
EXIT_CODE cmd_cd(char *name, Arg_t *arg);

/*!
 * @brief Command to display help
 *
 * @param name : Name of the command.
 * @param arg  : Argument to the command.
 *
 * @return Exit status
 */
EXIT_CODE cmd_help(char *name, Arg_t *arg);

/*!
 * @brief Command to see background jobs.
 *
 * @param name : Name of the command.
 * @param arg  : Argument to the command.
 *
 * @return Exit status
 */
EXIT_CODE cmd_jobs(char *name, Arg_t *arg);

/*!
 * @brief Command to exit.
 *
 * @param name : Name of the command.
 * @param arg  : Argument to the command.
 *
 * @return Exit status
 */
EXIT_CODE cmd_exit(char *name, Arg_t *arg);

/*!
 * @brief Command default command handler.
 *
 * @param name : Name of the command.
 * @param arg  : Argument to the command.
 *
 * @return Exit status
 */
EXIT_CODE cmd_default(char *name, Arg_t *arg);

/*!
 * @brief Command to report jobs.
 *
 * @param name : Name of the command.
 * @param arg  : Argument to the command.
 *
 * @return Exit status
 */
EXIT_CODE cmd_report_jobs(char *name, Arg_t *arg);

#endif
