/*!
 * @file: arguments.h
 * @author: Suraj Deuja
 */
#ifndef __CMD_ARGUMENTS_H
#define __CMD_ARGUMENTS_H

#include <stddef.h>

#define MAX_NUM_ARGS 4096

/*!
 * Structure to hold argument type
 */
typedef struct {
    char  **buf;          ///< Argument buffer
    size_t index;         ///< Argument buffer length. Buffer is dynamic
    int    isBg;          ///< Set if background job is requested.
} Arg_t;

/*!
 * @brief Creates a buffer for string with one extra character for
 * NULL
 *
 * @param len : length of the buffer size which is 1 less than allocated
 */
Arg_t *create_arg();

/*!
 * @brief parses argument
 *
 * @param line: string to parse
 *
 * @return Argument structure
 */
Arg_t *parse_arg(char *line);

/*!
 * @brief Frees argument
 *
 * @param arg: argument to be freed
 */
void free_arg(Arg_t *arg);

#endif
