/*!
 * @file: Job.h
 * @author: Suraj Deuja
 */
#ifndef __JOB_H
#define __JOB_H

#include "arguments.h"
#include "ourhdr.h"

/*!
 * Defines the jobs status.
 */
typedef enum {
    JOB_COMPLETED = 0,
    JOB_KILLED,
    JOB_CREATED,
    JOB_RUNNING,
    JOB_PAUSED,
    JOB_STOPPED,
} tJobStatus;

/*!
 * Defines data structure for job.
 */
typedef struct {
    char       *name;
    Arg_t      *arg;
    pid_t       pid;
    size_t      id;
    bool        is_reported;
    tJobStatus  status;
} Job_t;

/*!
 * @brief initializes the jobs List.
 */
void jobs_init();

/*!
 * @brief creates a new job.
 *
 * @return new empty job.
 */
Job_t *job_create();

/*!
 * @brief Sets the name of the job.
 *
 * @param job: job to set name.
 * @param name: name of the job.
 */
void job_set_name(Job_t *job, char *name);

/*!
 * @brief Adds job the job list.
 *
 * @param  job: job to add.
 */
void job_add(Job_t *job);

/*!
 * @brief Reports all jobs that has changed status.
 */
void jobs_report();

/*!
 * @brief Returns the current number of active jobs.
 *
 * @return Number of active jobs.
 */
int jobs_current();

/*!
 * @brief Frees a job.
 *
 * @param obj: job to free
 */
void free_job(void *obj);

/*!
 * @brief Lists all active jobs
 */
void jobs_list();

/*!
 * @brief Destroys all jobs. It does not kill the active processes.
 */
void jobs_destroy();

/*!
 * @brief Compares if two jobs are equal.
 *
 * @param job1: job to compare
 * @param job2: job to compare
 * @param 1 if equal and 0 otherwise
 */
int jobs_equal(const void *job1, const void *job2);

#endif
