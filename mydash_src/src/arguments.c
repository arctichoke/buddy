/*!
 * @file: arguments.c
 * @author: Suraj Deuja
 */
#include <stdlib.h>

#include "arguments.h"
#include "ourhdr.h"

/*!
 * @brief Creates a buffer for string with one extra character for
 * NULL
 * @param len : length of the buffer size which is 1 less than allocated
 */
static char *create_str_buffer(size_t len)
{
    char *buf = (char *)malloc((len + 1)*sizeof(char));
    return buf;
}

Arg_t *create_arg()
{
    Arg_t *newArg = (Arg_t *) malloc(sizeof(Arg_t));

    if (NULL != newArg)
    {
        //Allocate two extra for cmd name and background flag.
        newArg->buf = (char **) malloc(sizeof(char *) * (MAX_NUM_ARGS + 2));
        newArg->index = 0;
    }

    return newArg;
}

/*!
 * @brief adds argument to the Arg_t
 *
 * @param arg: argument structure
 * @param flag: argument to add
 */
static int add_arg(Arg_t *arg, char *flag)
{
    size_t len = strlen(flag);
    if (arg->index > MAX_NUM_ARGS)
    {
        return -1;
    }

    arg->buf[arg->index] = (char *)create_str_buffer(sizeof(char) * strlen(flag));
    strncpy(arg->buf[arg->index], flag, len);
    arg->buf[arg->index][len] = '\0';
    arg->buf[arg->index+1] = NULL;
    arg->index++;

    return 0;
}

Arg_t *parse_arg(char *line)
{
    char *token;
    size_t len;
    char *bg_flag;
    Arg_t *arg = create_arg();

    token = strtok(line, " ");
    while (token != NULL)
    {
        if (0 != add_arg(arg, token))
        {
            free_arg(arg);
            return NULL;
        }
        token = strtok(NULL, " ");
    }

    // Moves the pointer bg_flag to the last character
    // of last argument and checks for bacground flag
    len = strlen(arg->buf[arg->index -1]);
    bg_flag = arg->buf[arg->index - 1] + len -1;
    arg->isBg = !strcmp(bg_flag, "&");

    // If the background argument was passed remove it
    // from the argument and set background flag
    if (arg->isBg)
    {
        if (len == 1)
        {
            free(arg->buf[arg->index - 1]);
            arg->buf[arg->index - 1] = NULL;
            arg->index--;
        }
        else
        {
            *bg_flag = '\0';
            arg->buf[arg->index] = NULL;
        }
    }

    return arg;
}

void free_arg(Arg_t *arg)
{
    size_t i = 0;
    if (NULL != arg)
    {
        while (i < arg->index)
        {
            free(arg->buf[i]);
            i++;
        }

        if (NULL != arg->buf)
            free(arg->buf);

        free(arg);
    }
}
