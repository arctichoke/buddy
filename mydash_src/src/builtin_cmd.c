/*!
 * @file: builtin_cmd.c
 * @author: Suraj Deuja
 */
#include <stddef.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <limits.h>
#include <pwd.h>

#include "builtin_cmd.h"
#include "dash_types.h"
#include "arguments.h"
#include "job.h"


/*! Command handler table */
Cmd_t builtin_cmd_tbl[] = {{"cd", "change directeory", cmd_cd},
                           {"help", "display help", cmd_help},
                           {"jobs",  "prints all jobs", cmd_jobs},
                           {"exit", "exit from shell", cmd_exit},
                           {"report_jobs", "reports updated jobs", cmd_report_jobs},
                           {"default"," ", cmd_default}};

size_t CMD_TBL_SIZE = sizeof(builtin_cmd_tbl)/sizeof(builtin_cmd_tbl[0]);

EXIT_CODE cmd_cd(char *name, Arg_t *arg)
{
    char buf[64];               // User name length is 32 character max
    struct passwd *pw;
    char *path = arg->buf[1];

    if (NULL == path)
    {
        pw = getpwuid(getuid());
        sprintf(buf,"%s", pw->pw_dir);
        path = buf;
    }

    if (0 != chdir(path))
    {
        printf("Failed to change directory!\n");
    }

    free_arg(arg);
    return 0;
};

EXIT_CODE cmd_exit(char *name, Arg_t *arg)
{
    // Free all background jobs
    free_arg(arg);
    jobs_destroy();
    exit(EXIT_SUCCESS);
}

EXIT_CODE cmd_help(char *name, Arg_t *arg)
{
    size_t i;
    for (i = 0; i < CMD_TBL_SIZE; i++)
    {
        printf("%s:\t\t\t%s\n", builtin_cmd_tbl[i].name, builtin_cmd_tbl[i].desc);
    }

    free_arg(arg);
    return 0;
}

EXIT_CODE cmd_jobs(char *name, Arg_t *arg)
{
    if (jobs_current())
    {
        jobs_list();
    }

    free_arg(arg);
    return 0;
}

EXIT_CODE cmd_report_jobs(char *name, Arg_t *arg)
{
    jobs_report();
    free_arg(arg);
    return 0;
}

/*!
 * @brief Runs job in background.
 *
 * @param name : Name of the command.
 * @param arg  : Argument to the command.
 */
static EXIT_CODE run_background(char *name, Arg_t *arg)
{
    Job_t *job;
    int status = 0;
    pid_t child = fork();

    if (child == 0)
    {
       status = execvp(name, arg->buf);
       // Should only execute if the exec failed
       printf("Failed to execute command\n");
    }
    else
    {
        // Parent does not need to wait
        job = job_create();
        job_set_name(job, arg->buf[0]);

        job->pid = child;
        job->is_reported = 0;
        job->arg = arg;

        job_add(job);

        printf("[%zu]+ %d\n", job->id, child);
    }
    return status;
}

/*!
 * @brief Runs job in foreground.
 *
 * @param name : Name of the command.
 * @param arg  : Argument to the command.
 */
static EXIT_CODE run_foreground(char *name, Arg_t *arg)
{
    int status;
    pid_t child = fork();

    if (child == 0)
    {
       status = execvp(name, arg->buf);
       printf("No such commands!\n");
    }
    else
    {
        waitpid(child, &status, 0);
    }

    free_arg(arg);
    return status;
}

EXIT_CODE cmd_default(char *name, Arg_t *arg)
{
    if (arg->isBg)
        return run_background(name, arg);
    else
        return run_foreground(name, arg);
}

