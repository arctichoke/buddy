#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <sys/wait.h>

#include "List.h"
#include "Node.h"
#include "job.h"

struct list *gJobList;
static size_t job_id_max = 0;

void jobs_init()
{
    gJobList = createList(jobs_equal, NULL, free_job);
}

Job_t *job_create()
{
    Job_t *job = (Job_t *) malloc(sizeof(Job_t));
    return job;
}

void job_add(Job_t *job)
{
    struct node *node = createNode(job);
    job->id = job_id_max;
    job_id_max++;
    addAtFront(gJobList, node);
}

void job_set_name(Job_t *job, char *name)
{
    size_t len = strlen(name);
    job->name = (char *)malloc(sizeof(char) * (len + 1));
    strncpy(job->name, name, len);
    job->name[len] = '\0';
}

int jobs_current(void)
{
    return gJobList->size;
}

/*!
 * @brief Updates the job status.
 *
 * @param job : job whose status is to be updated.
 */
static void job_status_update(Job_t *job)
{
    int status;
    int end_id = waitpid(job->pid, &status, WNOHANG | WUNTRACED);
    if (-1 == end_id)
    {
        return;
    }
    else if (0 == end_id)
    {
        job->status = JOB_RUNNING;
    }
    else
    {
        // Status has changed.
        if (WIFEXITED(status))
        {
            job->status = JOB_COMPLETED;
            job->is_reported = FALSE;
        }
        else if(WIFSIGNALED(status))
        {
            job->status = JOB_KILLED;
            job->is_reported = FALSE;
        }
        else if (WIFSTOPPED(status))
        {
            job->status = JOB_STOPPED;
            job->is_reported = FALSE;
        }
        else if (WIFCONTINUED(status))
        {
            job->status = JOB_RUNNING;
            job->is_reported = FALSE;
        }
    }
}

static char *job_status_str(tJobStatus status)
{
    switch (status)
    {
        case JOB_COMPLETED:
            return "Done";
        case JOB_KILLED:
            return "Killed";
        case JOB_STOPPED:
            return "Stopped";
        case JOB_RUNNING:
            return "Running";
        default:
            return "Unknown";
    }
}

/*!
 * @brief Removes job from the list.
 *
 * @param job: job to remove.
 */
static struct node * job_remove(Job_t *job)
{
    struct node *job_node = search(gJobList, job);
    return removeNode(gJobList, job_node);
}

void remove_completed_jobs()
{
    Job_t *job;
    struct node *start = gJobList->head;
    struct node *temp_node;

    while (start != NULL)
    {
        job = (Job_t *) start->obj;
        job_status_update(job);
        if (job->status == JOB_COMPLETED && job->is_reported)
        {
            temp_node = job_remove(job);
            freeNode(temp_node, free_job);
            start = gJobList->head;
            continue;
        }
        start = start->next;
    }

    if (gJobList->size == 0)
        job_id_max = 0;

}

void jobs_report(void)
{
    struct node *start;
    Job_t *job;
    size_t i;

    remove_completed_jobs();
    start = gJobList->head;

    while (start != NULL)
    {
        job = (Job_t *) start->obj;
        if (!job->is_reported)
        {
            printf("[%zu]+ %s ", job->id, job_status_str(job->status));
            for(i = 0; i < job->arg->index; i++)
            {
                printf("%s ", job->arg->buf[i]);
            }
            printf("&\n");
            job->is_reported = TRUE;

        }
        start = start->next;
    }
}

void jobs_list(void)
{
    struct node *start;
    Job_t *job;
    size_t i;

    remove_completed_jobs();
    start = gJobList->head;

    while (start != NULL)
    {
        job = (Job_t *) start->obj;
        printf("[%zu]+ %s ", job->id, job_status_str(job->status));
        for(i = 0; i < job->arg->index; i++)
        {
            printf("%s ", job->arg->buf[i]);
        }
        printf("&\n");

        job->is_reported  = TRUE;
        start = start->next;
    }
}

int jobs_equal(const void *job1, const void *job2)
{
    Job_t *j1 = (Job_t *) job1;
    Job_t *j2 = (Job_t *) job2;

    return j1->pid == j2->pid;
}

void jobs_destroy()
{
    freeList(gJobList);
}

void free_job(void *obj)
{
    Job_t * job = (Job_t *) obj;
    if (job != NULL)
    {
        if (job->name != NULL)
        {
            free(job->name);
        }

        if (job->arg != NULL);
        {
            free_arg(job->arg);
        }

        free(job);
    }
}
